#!/usr/bin/env bash
export WORKSPACE=~/vsphere-dependencies-install
cd ${WORKSPACE}
sudo apt-get update && sudo apt-get upgrade

sudo apt-get install -y nano jq unzip wget tar curl
sh -c "$(curl -fsSL https://raw.githubusercontent.com/Linuxbrew/install/master/install.sh)"
test -d ~/.linuxbrew &&PATH="$HOME/.linuxbrew/bin:$HOME/.linuxbrew/sbin:$PATH"
test -d /home/linuxbrew/.linuxbrew &&PATH="/home/linuxbrew/.linuxbrew/bin:/home/linuxbrew/.linuxbrew/sbin:$PATH"
test -r ~/.bash_profile && echo "export PATH='$(brew --prefix)/bin:$(brew--prefix)/sbin'":'"$PATH"' >>~/.bash_profile
echo "export PATH='$(brew --prefix)/bin:$(brew --prefix)/sbin'":'"$PATH"'>>~/.profile
brew install hello

## Install bosh
wget https://github.com/cloudfoundry/bosh-cli/releases/download/v6.3.1/bosh-cli-6.3.1-linux-amd64
chmod +x bosh-cli-6.3.1-linux-amd64
sudo mv bosh-cli-6.3.1-linux-amd64 /usr/local/bin/bosh
sudo apt-get install -y build-essential zlibc zlib1g-dev ruby ruby-dev openssl libxslt1-dev libxml2-dev libssl-dev libreadline7 libreadline-dev libyaml-dev libsqlite3-dev sqlite3


sudo wget https://github.com/cloudfoundry/bosh-bootloader/releases/download/v8.4.0/bbl-v8.4.0_linux_x86-64
chmod +x bbl-v8.4.0_linux_x86-64
sudo mv bbl-v8.4.0_linux_x86-64 /usr/local/bin/bbl

##install kubectl
curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.15.5/bin/linux/amd64/kubectl
chmod +x ./kubectl
sudo mv ./kubectl /usr/local/bin/kubectl
source <(kubectl completion bash)
echo "source <(kubectl completion bash)" >> ~/.bashrc


## Instal helm
wget https://storage.googleapis.com/kubernetes-helm/helm-v2.13.1-linux-amd64.tar.gzsudo tar -xvf helm-v2.13.1-linux-amd64.tar.gz
chmod +x linux-amd64/helm
sudo mv linux-amd64/helm /usr/local/bin/helm
source <(helm completion bash)
echo "source <(helm completion bash)" >> ~/.bashrc
sudo rm -rf linux-amd64
suro rm -rf helm-v2.13.1-linux-amd64.tar.gz

##Install credhub
wget https://github.com/cloudfoundry-incubator/credhub-cli/releases/download/2.4.0/credhub-linux-2.4.0.tgz
sudo tar -xvf credhub-linux-2.4.0.tgz
chmod +x credhub
sudo mv credhub /usr/local/bin/credhub
sudo rm -rf credhub-linux-2.4.0.tgz

##install python software properties
sudo apt-get install curl python-software-properties
sudo curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -

## install nodejs, npm and tree packages
sudo apt install -y nodejs
sudo apt install -y node-abbrev node-ansi node-ansi-color-table node-archy node-block-stream node-fstream node-fstream-ignore node-github-url-from-git node-glob node-graceful-fs node-inherits node-ini node-lockfile node-lru-cache node-minimatch node-mkdirp node-gyp node-nopt node-npmlog node-once node-osenv node-read node-read-package-json node-request node-retry node-rimraf node-semver node-sha node-slide node-tar node-underscore node-which
sudo apt-get install -f npm -y
sudo apt install -y tree
git clone https://github.com/twhitbeck/nanoid-cli.git
cd ${WORKSPACE}/nanoid-cli/
sudo npm install -g

##test nanoid
nanoid

cd ${WORKSPACE}
sleep 5s


sudo apt-get update && sudo apt-get upgrade

nano --version
jq --version
bosh -v
bbl version
credhub --version
kubectl version --client
helm version --client
cd ~
echo "-----------------------------------------------------------------"
echo "ALL DONE!"